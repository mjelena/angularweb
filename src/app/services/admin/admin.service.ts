import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,  HttpClientModule, HttpErrorResponse, HttpResponse, HttpResponseBase} from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { _throw } from 'rxjs/observable/throw';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import {User} from './user.model';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const token = localStorage.getItem('token')
  ? '?token=' + localStorage.getItem('token')
  : '';

@Injectable()
export class AdminService {

  private user: User[];
  private apiUrl = 'http://localhost:3000/api/users';
  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.apiUrl + token, httpOptions)
    .pipe( catchError(response => {
      if (response instanceof HttpErrorResponse) {

        // Check if this error has a 2xx status
        if (this.is2xxStatus(response)) {

          // Convert the error to a standard response with a null body and then return
          return of(new HttpResponse({
            headers: response.headers,
            status: response.status,
            statusText: response.statusText,
            url: response.url
          }));
        }
        // This is a real error, rethrow
        return _throw(response);
      }
    }));

}

private is2xxStatus(response: HttpResponseBase) {
  return response.status >= 200 && response.status < 300 ;
}

putAdmin(userId, body) {
  console.log(userId);
  return this.http.put(this.apiUrl + '/' + userId + token, body , httpOptions)
  .pipe(map(res => JSON.stringify(res)));
}

deleteUser(userId){
  return this.http.delete(this.apiUrl + '/' + userId + token, httpOptions )
  .pipe(map(res => JSON.stringify(res)));
}

}
