export class User {
    constructor(
        public name: String,
        public email: String,
        public admin: Boolean,
        public _id: String,
        public mainAdmin: Boolean
    ) {}
}