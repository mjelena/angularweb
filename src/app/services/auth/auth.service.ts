import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,  HttpClientModule, HttpErrorResponse, HttpResponse, HttpResponseBase} from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { _throw } from 'rxjs/observable/throw';
import { tokenNotExpired } from 'angular2-jwt';




const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AuthService {
  authToken: any;
  user: any;
  username: String;
  admin: boolean;
  mainAdmin: boolean;
  private apiUrl = 'http://localhost:3000/api/';

  constructor(private http: HttpClient
              ) { }

  registerUser(user) {
    this.user = JSON.stringify(user);
    console.log(this.user);
    return this.http.post(this.apiUrl + 'register', this.user, httpOptions)
      .pipe(map(res  => JSON.stringify(res)));
  }

  loginUser(user) {
    this.user = JSON.stringify(user);
    return this.http.post(this.apiUrl + 'login', this.user, httpOptions)
      .pipe( catchError(response => {
        if (response instanceof HttpErrorResponse) {

      // Check if this error has a 2xx status
      if (this.is2xxStatus(response)) {

        // Convert the error to a standard response with a null body and then return
        return of(new HttpResponse({
          headers: response.headers,
          status: response.status,
          statusText: response.statusText,
          url: response.url
        }));
      }



    // This is a real error, rethrow
    return _throw(response);
      }
  }));
  }
  private is2xxStatus(response: HttpResponseBase) {
      return response.status >= 200 && response.status < 300 ;
    }

  storeUserToken(username, token, admin, mainAdmin) {

    localStorage.setItem('token', token);
    localStorage.setItem('username', username);
    localStorage.setItem('admin', admin);
    localStorage.setItem('mainAdmin', mainAdmin);

    this.authToken = token;
    this.username = username;
    this.admin = admin;
    this.mainAdmin = mainAdmin;

  }

  loadToken() {
    const token = localStorage.getItem('token');
    this.authToken = token;
  }

  logout() {
    this.authToken = null;
    this.username = null;
    this.admin = null;
    this.mainAdmin = null;
    localStorage.clear();
  }

  loggedIn() {
    return tokenNotExpired();
  }

  loggedAdmin() {
    const adminTrue = (localStorage.getItem('admin') === 'true') ? true : false;
    return (adminTrue);
  }

}
