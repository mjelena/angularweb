export class Photo {
    constructor(
       public description: String,
       public photo: File,
       public path: String,
       public _id: String,
       public author: String,
       public name: String
    ) {}
}