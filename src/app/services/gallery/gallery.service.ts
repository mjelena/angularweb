import { Injectable } from '@angular/core';
import {Photo} from '../gallery/photo.model';
import { HttpClient, HttpHeaders,  HttpClientModule, HttpErrorResponse, HttpResponse, HttpResponseBase} from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { _throw } from 'rxjs/observable/throw';
import 'rxjs/add/operator/catch';
import { City } from '../city/city.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const token = localStorage.getItem('token')
  ? '?token=' + localStorage.getItem('token')
  : '';

@Injectable()
export class GalleryService {

  private apiUrl = 'http://localhost:3000/api/cities/';

  constructor(private http: HttpClient) { }

  postPhoto(photo: File, cityId) {
    console.log(photo);
    const formData = new FormData();
    formData.append('file', photo, photo.name);
    console.log(formData);
    return this.http.post(this.apiUrl + cityId + '/photo' + token, formData)
    .pipe(map(res => JSON.stringify(res)));

  }

  getAllPhotos(cityId): Observable<City[]> {
    return this.http.get<City[]>(this.apiUrl + cityId + token)
    .pipe(catchError(response => {
      if (response instanceof HttpErrorResponse) {

        // Check if this error has a 2xx status
        if (this.is2xxStatus(response)) {

          // Convert the error to a standard response with a null body and then return
          return of(new HttpResponse({
            headers: response.headers,
            status: response.status,
            statusText: response.statusText,
            url: response.url
          }));
        }
        // This is a real error, rethrow
        return _throw(response);
      }
    }));
}

private is2xxStatus(response: HttpResponseBase) {
  return response.status >= 200 && response.status < 300 ;
}

deletePhoto(photoid, cityid) {
  return this.http.delete(this.apiUrl + cityid + '/photo/' + photoid + token)
  .pipe(map(res => JSON.stringify(res)));
}

}
