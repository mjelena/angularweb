export  class Comment {
  constructor(
    public reviewText: String,
    public rating: number,
    public author: String,
    public createdOn: String,
    public _id: String,
    public authorId: String
  ) {}
}
