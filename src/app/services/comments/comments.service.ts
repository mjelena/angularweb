import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,  HttpClientModule, HttpErrorResponse, HttpResponse, HttpResponseBase} from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { _throw } from 'rxjs/observable/throw';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';
import {Comment} from '../comments/commet.model';
import {ActivatedRoute} from '@angular/router';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const token = localStorage.getItem('token')
  ? '?token=' + localStorage.getItem('token')
  : '';

@Injectable()
export class CommentsService {

  private comment: any;
  private apiUrl = 'http://localhost:3000/api/cities/' ;
  constructor(private http: HttpClient, private route: ActivatedRoute) {}

  getComments(cityid): Observable<Comment[]> {
    return this.http.get<Comment[]>(this.apiUrl + cityid  + '/reviews' + token)
      .pipe(catchError(response => {
        if (response instanceof HttpErrorResponse) {

          // Check if this error has a 2xx status
          if (this.is2xxStatus(response)) {

            // Convert the error to a standard response with a null body and then return
            return of(new HttpResponse({
              headers: response.headers,
              status: response.status,
              statusText: response.statusText,
              url: response.url
            }));
          }
          // This is a real error, rethrow
          return _throw(response);
        }
      }));
  }
  private is2xxStatus(response: HttpResponseBase) {
    return response.status >= 200 && response.status < 300 ;
  }

  public addComment(comment, cityid) {
    this.comment = JSON.stringify(comment);
    console.log(this.comment);
   return this.http.post(this.apiUrl + cityid + '/reviews' + token, this.comment , httpOptions)
      .pipe(map(res => JSON.stringify(res)));

  }

  public deleteComment(commentId, cityId) {
    return this.http.delete(this.apiUrl + cityId + '/reviews/' + commentId + token, httpOptions)
    .pipe(map( res => JSON.stringify(res)));
  }

}
