import {AbstractControl} from '@angular/forms';


export class EmailValidator {

  public static validate(c: AbstractControl) {
    const re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    return c.value.match(re) ? null : {'invalidEmailAddress': true};

  }
}
