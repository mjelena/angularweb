import {AbstractControl, FormGroup} from '@angular/forms';

export class PasswordsMatchValidator {

  public static validate(password, passwordConfirm) {
    return (c: FormGroup) => {

        return (c.controls && c.controls[password].value === c.controls[passwordConfirm].value) ? null : {
          passwordsEqual: {
            valid: false
          }
        };
      };

  }
}

