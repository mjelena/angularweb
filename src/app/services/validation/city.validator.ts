
import {AbstractControl} from '@angular/forms';


export class CityValidator {

  public static validate(c: AbstractControl) {
    const re = /^([a-zA-Z\u0080-\u024F]+(?:. |-| |'))*[a-zA-Z\u0080-\u024F]*$/;
    return c.value.match(re) ? null : {'invalidEmailAddress': true};

  }
}
