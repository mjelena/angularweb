import {AbstractControl} from '@angular/forms';


export class NoWhitespaceValidator {

  public static validate(c: AbstractControl) {
    const isWhitespace = (c.value || '').trim().length === 0;
    const hasWhitespace = !isWhitespace ? c.value.includes(' ') : true;
    const isValid = !isWhitespace && !hasWhitespace;
    return isValid ? null : { 'whitespace': true };

  }
}




