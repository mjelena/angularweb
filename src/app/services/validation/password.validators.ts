import {AbstractControl} from '@angular/forms';


export class PasswordValidator {

  public static validate(c: AbstractControl) {
        // {6,100}           - Assert password is between 6 and 100 characters
        // (?=.*[0-9])       - Assert a string has at least one number
        return c.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,20}$/) ? null : { 'invalidPassword': true };
  }
}
