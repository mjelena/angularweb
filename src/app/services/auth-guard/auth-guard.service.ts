import { Injectable } from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {AuthService} from '../auth/auth.service';
import {FlashMessagesService} from 'angular2-flash-messages';



@Injectable()
export class AuthGuardService implements  CanActivate{



  constructor(private authService: AuthService,
              private router: Router,
              private flashMessages: FlashMessagesService) { }


  canActivate() {

    if (localStorage.getItem('token') && localStorage.getItem('username') && this.authService.loggedIn()) {
      return true;
    }
    if (localStorage.getItem('token') && localStorage.getItem('userId') && !this.authService.loggedIn()) {
      this.flashMessages.show('Your secure token expired!', {cssClass: 'alert-danger', timeout: 5000})
      this.authService.logout();
      this.router.navigate(['/login']);
      return false;
    }

    this.flashMessages.show('Make sure that you are logged in in order to see this page!', {cssClass: 'alert-info', timeout: 5000})
    this.router.navigate(['/login']);
    return false;
  }

}
