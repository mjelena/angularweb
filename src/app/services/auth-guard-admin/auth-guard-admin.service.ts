import { Injectable } from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {AuthService} from '../auth/auth.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {ifTrue} from 'codelyzer/util/function';

@Injectable()
export class AuthGuardAdminService {


  constructor(private authService: AuthService,
              private router: Router,
              private flashMessages: FlashMessagesService) { }


  canActivate() {
    const adminTrue = (localStorage.getItem('admin') === 'true') ? true : false;

    if (localStorage.getItem('admin')) {
        return true;
    }

    /*

    if (localStorage.getItem('token') && localStorage.getItem('username') && localStorage.getItem('admin') && adminTrue && this.authService.loggedIn()) {
      return true;
    }
    if (localStorage.getItem('token') && localStorage.getItem('userId') && localStorage.getItem('admin') && !this.authService.loggedIn()) {
      this.flashMessages.show('Your secure token expired!', {cssClass: 'alert-danger', timeout: 5000})
      this.authService.logout();
      this.router.navigate(['/login']);
      return false;
    }

    this.flashMessages.show('Only admin can access!', {cssClass: 'alert-danger', timeout: 5000});
    this.router.navigate(['/home']);
    return false;*/
  }

}
