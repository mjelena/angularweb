import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,  HttpClientModule, HttpErrorResponse, HttpResponse, HttpResponseBase} from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { _throw } from 'rxjs/observable/throw';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';
import {City} from './city.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const token = localStorage.getItem('token')
  ? '?token=' + localStorage.getItem('token')
  : '';

@Injectable()
export class CityService {

  public city: any;

  private apiUrl = 'http://localhost:3000/api/cities';

  constructor(private http: HttpClient) { }

  public addCity(city) {

    this.city = JSON.stringify(city);
    console.log(this.city);
    return this.http.post(`${this.apiUrl}${token}`, this.city, httpOptions)
      .pipe(map(res  => JSON.stringify(res)));
  }
/*
  getCities(): Observable<City[]> {
    return this.http.get<City[]>(this.apiUrl)
      .pipe(map(res => JSON.stringify(res)))
      .catch((error: Response) => {
        return Observable.throw(error.json());
      });
}*/

  getCities(): Observable<City[]> {
    return this.http.get<City[]>(this.apiUrl)
      .pipe( catchError(response => {
        if (response instanceof HttpErrorResponse) {

          // Check if this error has a 2xx status
          if (this.is2xxStatus(response)) {

            // Convert the error to a standard response with a null body and then return
            return of(new HttpResponse({
              headers: response.headers,
              status: response.status,
              statusText: response.statusText,
              url: response.url
            }));
          }
          // This is a real error, rethrow
          return _throw(response);
        }
      }));
  }
  private is2xxStatus(response: HttpResponseBase) {
    return response.status >= 200 && response.status < 300 ;
  }

  public getCity(id) {
    return this.http.get(this.apiUrl + '/' + id + token ).pipe( catchError(response => {
      if (response instanceof HttpErrorResponse) {

        // Check if this error has a 2xx status
        if (this.is2xxStatus(response)) {

          // Convert the error to a standard response with a null body and then return
          return of(new HttpResponse({
            headers: response.headers,
            status: response.status,
            statusText: response.statusText,
            url: response.url
          }));
        }
        // This is a real error, rethrow
        return _throw(response);
      }
    }));
    }

deleteCity(cityId) {
    return this.http.delete(this.apiUrl + '/' + cityId + token, httpOptions)
    .pipe(map(res => JSON.stringify(res)));


}

updateCity(cityid, city) {
  return this.http.put(this.apiUrl + '/' + cityid + token, JSON.stringify(city), httpOptions)
  .pipe(map(res => JSON.stringify(res)));
}

}
