import {Place} from './place.model';
import {Comment} from '../comments/commet.model';
import {Photo} from '../gallery/photo.model';

export class City {
  constructor(public name: string,
              public country: string,
              public information?: string,
              public lat?: number,
              public _id?: string,
              public lng?: number,
              public rating?: number,
              public places?: Place[],
              public comments?: Comment[],
              public pictures?: Photo[]
  ) {}
}
