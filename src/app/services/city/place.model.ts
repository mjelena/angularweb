export class Place {
  constructor(
    public place: String,
    public author: String,
    public _id: String
  ) {}
}
