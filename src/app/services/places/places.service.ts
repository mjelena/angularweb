import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,  HttpClientModule, HttpErrorResponse, HttpResponse, HttpResponseBase} from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { _throw } from 'rxjs/observable/throw';
import { City } from '../city/city.model';
//import {ActivatedRoute} from "@angular/router";
import {Observable} from 'rxjs/Observable';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8',
  'Accept' : 'application/json' })
};

const token = localStorage.getItem('token')
? '?token=' + localStorage.getItem('token')
: '';

@Injectable()
export class PlacesService {

 
  public place: any;

  private apiUrl = 'http://localhost:3000/api/cities/';

  constructor(private http: HttpClient,
             ) {
              }

   public addPlace(place, cityId): Observable<City> {

     console.log(place);
     console.log(cityId);
    console.log(this.place);
    return this.http.post<City>(this.apiUrl + cityId + '/places' + token, {'place': place}, httpOptions )
      .pipe(catchError(response => {
        if (response instanceof HttpErrorResponse) {

          // Check if this error has a 2xx status
          if (this.is2xxStatus(response)) {

            // Convert the error to a standard response with a null body and then return
            return of(new HttpResponse({
              headers: response.headers,
              status: response.status,
              statusText: response.statusText,
              url: response.url
            }));
          }
          // This is a real error, rethrow
          return _throw(response);
        }
      }));
  }
  private is2xxStatus(response: HttpResponseBase) {
    return response.status >= 200 && response.status < 300 ;
  }

  deletePlace(placeid, cityid) {
    return this.http.delete(this.apiUrl + cityid + '/places/' + placeid + token, httpOptions)
    .pipe(map((res) => JSON.stringify(res)));
  }

  updatePlace(cityid, place, placeid) {
  return this.http.put(this.apiUrl + cityid + '/places/' + placeid + token, {'place': place}, httpOptions)
  .pipe(map(res => JSON.stringify(res)));

  }
}
