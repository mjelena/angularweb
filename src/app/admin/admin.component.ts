import { Component, OnInit } from '@angular/core';
import {AdminService} from '../services/admin/admin.service';
import {User} from '../services/admin/user.model';
import {FlashMessagesService} from 'angular2-flash-messages';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

    public users: User[];
    public checked;
    private userId;

  constructor(public adminService: AdminService,
    private modalService: NgbModal,
    private flashMessages: FlashMessagesService,
    private router: Router) { }

  ngOnInit()
  {
    this.adminService.getUsers().subscribe((res) => {
       this.users = res;
       console.log(this.users);
       
    }, (error) => {
      this.flashMessages.show('Error!', {cssClass: 'alert-danger', timeout: 5000});
    });
  }

  isMainAdmin() {
    return localStorage.getItem('mainAdmin') && localStorage.getItem('mainAdmin') === 'true';
  }

  changeState(e, userId) {
    if (e.target.checked) {
      const body = {'admin' : 'true'};
      this.adminService.putAdmin(userId, body).subscribe((res) => {

      }, (err) => {
        this.flashMessages.show('Error! Try again!', {cssClass: 'alert-danger', timeout: 5000});
      });

    } else if (!e.target.checked) {
      const body = {'admin' : 'false'};
       this.adminService.putAdmin(userId, body).subscribe((res) => {

       }, (err) => {

       });
    }

  }

  openModalDelete(content, userId) {
    this.userId = userId;
 
     this.modalService.open(content).result.then((result) => {
       
     }, (reason) => {
       if (reason === 'YES') {
         console.log('Comment ID: '  + userId);
         this.adminService.deleteUser(userId).subscribe((res) => {
           this.flashMessages.show('You delete a user!', {cssClass: 'alert-success', timeout: 5000});
           this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(()=>
 this.router.navigate(['/admin']));
         }, (err) => {
           this.flashMessages.show('Error. Try again!', {cssClass: 'alert-danger', timeout: 5000});
         });
         console.log('Klinkut je YES');
         return;
       }
       // Click na iks
       console.log('discarded ' + reason);
       
     });
   }

}
