import { Component, OnInit } from '@angular/core';
import {GalleryService} from '../services/gallery/gallery.service';
import {FormGroup, FormControl, Validators, AbstractControl, FormBuilder} from '@angular/forms';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';
import {Photo} from '../services/gallery/photo.model';
import {City} from '../services/city/city.model';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import {FlashMessagesService} from 'angular2-flash-messages';
import {Router} from '@angular/router';

import {
  AccessibilityConfig, Action, AdvancedLayout, ButtonEvent, ButtonsConfig, ButtonsStrategy, ButtonType, Description, DescriptionStrategy,
  DotsConfig, GridLayout, Image, ImageModalEvent, LineLayout, PlainGalleryConfig, PlainGalleryStrategy, PreviewConfig
} from 'angular-modal-gallery';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css', '../../public/js/amelia.bootstrap.css', '../../public/css/chocolat.css']
})
export class GalleryComponent implements OnInit {

  fileToUpload: File = null;
  cityId: any;
  public photos: Photo[];
  public cities: City[];
  public photoP: Photo; 

  public formAddPhoto: FormGroup;
  public description: AbstractControl;
  public photo: AbstractControl;

  constructor(private formBuilder: FormBuilder,
    private galleryService: GalleryService,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private sanitization: DomSanitizer,
    private flashMessages: FlashMessagesService,
    private router: Router,) {
      this.formAddPhoto = this.formBuilder.group({
        photo:  new FormControl('', Validators.compose([Validators.required])),
      });
      this.photo = this.formAddPhoto.controls['photo'];
     }

  ngOnInit() {
    this.cityId = this.route.snapshot.params['id'];

    this.galleryService.getAllPhotos(this.cityId).subscribe((res) => {
      console.log(res);
      this.cities = res;
      this.photos = res['pictures'];
      console.log(this.photos);
    }, (err) => {

    });
  }

  
isOwner(author) {
  return (localStorage.getItem('username') &&  author === localStorage.getItem('username')) || (localStorage.getItem('admin') && localStorage.getItem('admin') === 'true');

}

isAdmin() {
  return localStorage.getItem('admin') && localStorage.getItem('admin') === 'true';
}

  handleFileInput(event) {
    this.fileToUpload = <File> event.target.files[0];
}

onAddPhotoSubmit(value) {
  const extension = this.fileToUpload.name.substring(
    this.fileToUpload.name.lastIndexOf('.') + 1).toLowerCase();

//The file uploaded is an image

if (extension === 'gif' || extension === 'png' || extension === 'bmp'
    || extension === 'jpeg' || extension === 'jpg') {
  this.galleryService.postPhoto(this.fileToUpload, this.cityId).subscribe(data => {
    this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(() =>
  this.router.navigate(['/city/' + this.cityId + '/photo']));
    console.log(data);
    }, error => {
      this.flashMessages.show('Error. Try again!', {cssClass: 'alert-danger', timeout: 5000});
    });
  } else {
    this.flashMessages.show('This file is not a image!', {cssClass: 'alert-danger', timeout: 5000});
  }
}


openModalAddPhoto(contentPhoto)
{
  this.modalService.open(contentPhoto).result.then((result) => {
  }, (err) => {
    this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(() =>
    this.router.navigate(['/city/' + this.cityId + '/photo']));
      });
      return;
    }

    private openModalDeletePhoto(contentDeletePhoto, photoId) {
      this.modalService.open(contentDeletePhoto).result.then((result) => {
      }, (reason) => {
        if (reason === 'YES') {
          this.galleryService.deletePhoto(photoId, this.cityId).subscribe((res) => {
            this.flashMessages.show('You deleted a photo!', {cssClass: 'alert-success', timeout: 5000});
            this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(() =>
  this.router.navigate(['/city/' + this.cityId + '/photo']));
          }, (err) => {
            this.flashMessages.show('Error. Try again!', {cssClass: 'alert-danger', timeout: 5000});
          });

          return;
        }
      });
    }

    private openModalListPhoto(contentListPhoto, photoId) {
      this.modalService.open(contentListPhoto, { size: 'lg' }).result.then((result) => {
      },
       (err) => {
            this.flashMessages.show('Error. Try again!', {cssClass: 'alert-danger', timeout: 5000});
          });

          return;
      }
}

  
