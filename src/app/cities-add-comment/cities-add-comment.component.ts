import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators, AbstractControl, FormBuilder} from '@angular/forms';
import {CityService} from '../services/city/city.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {CommentsService} from '../services/comments/comments.service';
import {Comment} from '../services/comments/commet.model';
import {Router} from '@angular/router';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-cities-add-comment',
  templateUrl: './cities-add-comment.component.html',
  styleUrls: ['../../public/css/util.css', '../../public/css/main.css', './cities-add-comment.component.css']
})
export class CitiesAddCommentComponent implements OnInit {


  private id;
  public formAddComment: FormGroup;


  rating: AbstractControl;
  reviewText: AbstractControl;
  currentRate: AbstractControl;
  constructor(private formBuilder: FormBuilder,
              private commentService: CommentsService,
              private router: Router,
              private flashMessages: FlashMessagesService,
              private route: ActivatedRoute) {
    this.formAddComment = this.formBuilder.group({
      reviewText:  new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(500)]))
  });

    this.reviewText = this.formAddComment.controls['reviewText'];
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
  }

  onAddCommentSubmit(values: any) {
    const addComment = {
      reviewText: values.reviewText,
      rating: this.currentRate
    };
    this.commentService.addComment(addComment, this.id).subscribe((res) => {
         console.log(res);
    }, (err) => {
      this.flashMessages.show('Error. Please try again.', {cssClass: 'alert-danger', timeout: 5000});
    }, () => {
      this.flashMessages.show('You add a comment!', {cssClass: 'alert-success', timeout: 5000});
      this.router.navigate(['/city/' + this.id]);
    });
  }



}
