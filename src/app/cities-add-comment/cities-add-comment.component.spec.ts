import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitiesAddCommentComponent } from './cities-add-comment.component';

describe('CitiesAddCommentComponent', () => {
  let component: CitiesAddCommentComponent;
  let fixture: ComponentFixture<CitiesAddCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitiesAddCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitiesAddCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
