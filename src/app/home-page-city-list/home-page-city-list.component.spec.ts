import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePageCityListComponent } from './home-page-city-list.component';

describe('HomePageCityListComponent', () => {
  let component: HomePageCityListComponent;
  let fixture: ComponentFixture<HomePageCityListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePageCityListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageCityListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
