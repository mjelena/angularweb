import { Component, OnInit } from '@angular/core';
import {CityService} from '../services/city/city.service';
import {City} from '../services/city/city.model';
import {FlashMessagesService} from 'angular2-flash-messages';


@Component({
  selector: 'app-home-page-city-list',
  templateUrl: './home-page-city-list.component.html',
  styleUrls: ['../../public/css/main.css', '../../public/css/util.css', './home-page-city-list.component.css']
})
export class HomePageCityListComponent implements OnInit {

  cities: City[];
  constructor(private cityService: CityService,
              private flashMessages: FlashMessagesService) { }

  ngOnInit() {
    this.cityService.getCities()
      .subscribe(cities => this.cities = cities, error => this.flashMessages.show('Error!', {cssClass: 'alert-danger', timeout: 5000}));
  }

  proveriGrad(city: City) {
    console.log(city);
  }

}
