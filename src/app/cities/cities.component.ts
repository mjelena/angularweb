import { Component, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute} from '@angular/router';
import {CityService} from '../services/city/city.service';
import {City} from '../services/city/city.model';
import { } from '@types/googlemaps';
import {Comment} from '../services/comments/commet.model';
import {CommentsService} from '../services/comments/comments.service';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FlashMessagesService} from 'angular2-flash-messages/module/flash-messages.service';
import {Router} from '@angular/router';
import {FormGroup, FormControl, Validators, AbstractControl, FormBuilder} from '@angular/forms';
import {PlacesService} from '../services/places/places.service';
import { DatePipe } from '@angular/common';
import {CityValidator} from '../services/validation/city.validator';

const token = localStorage.getItem('token');

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['../../public/js/amelia.bootstrap.css', '../cities/cities.component.css'/*, '../../public/css/main.css', '../../public/css/util.css'*/]
})
export class CitiesComponent implements OnInit {
  closeResult: string;
  commentId: any;

  public city: City;
  private cityId;
  public loaded: Boolean = false;
  public comments: Comment[];


  public formCityUpdate: FormGroup;
  public name: AbstractControl;
  public country: AbstractControl;
  public information: AbstractControl;
  public lat: AbstractControl;
  public lng: AbstractControl;
  public coordinates: FormGroup;

  public formAddPlace: FormGroup;
  public place: AbstractControl;
  public placeId: String;


  constructor(
    private cityService: CityService,
    private route: ActivatedRoute,
    private commentService: CommentsService,
    private modalService: NgbModal,
    private activeModal: NgbActiveModal,
    private flashMessages: FlashMessagesService,
    private router: Router,
    private formBuilder: FormBuilder,
    private placesService: PlacesService
  ) {
    this.formCityUpdate = this.formBuilder.group({
      name:  new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(70), CityValidator.validate])),
      country:  new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(70), CityValidator.validate])),
      information:  new FormControl('', Validators.compose([Validators.maxLength(3000)])),
      coordinates: this.formBuilder.group({
        lat: new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(10)])),
        lng: new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(10)])),
      })
    });

    this.name = this.formCityUpdate.controls['name'];
    this.country = this.formCityUpdate.controls['country'];
    this.information = this.formCityUpdate.controls['information'];
    this.coordinates = <FormGroup> this.formCityUpdate.controls['coordinates'];
    this.lat = this.coordinates.controls['lat'];
    this.lng = this.coordinates.controls['lng'];

    this.formAddPlace = this.formBuilder.group({
      place:  new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(70), CityValidator.validate]))
    });

    this.place = this.formAddPlace.controls['place'];
  }

  ngOnInit() {
    this.cityId = this.route.snapshot.params['id'];
    console.log('ID grada je ' + this.cityId);

    this.cityService.getCity(this.cityId).subscribe((res) => {
      this.city = res;
      console.log(this.city);
      this.loaded = true;
    }, (err) => {
      console.log(err);
    });

    this.commentService.getComments(this.cityId).subscribe((res) => {
      this.comments = res;
      console.log('\n GET COMMENTS SUCESS \n' + JSON.stringify(res));
    }, (err) => {
      console.log(err);
      }
    );
  }

isOwner(author) {
  return (localStorage.getItem('username') &&  author === localStorage.getItem('username')) || (localStorage.getItem('admin') && localStorage.getItem('admin') === 'true');

}

isAdmin() {
  return localStorage.getItem('admin') && localStorage.getItem('admin') === 'true';
}

  openModalDelete(content, commentId) {
   this.commentId = commentId;

    this.modalService.open(content).result.then((result) => {
      
    }, (reason) => {
      if (reason === 'YES') {
        console.log('Comment ID: '  + commentId);
        this.commentService.deleteComment(commentId, this.cityId).subscribe((res) => {
          this.flashMessages.show('You deleted a comment!', {cssClass: 'alert-success', timeout: 5000});
          this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(() =>
this.router.navigate(['/city/' + this.cityId]));
        }, (err) => {
          this.flashMessages.show('Error. Try again!', {cssClass: 'alert-danger', timeout: 5000});
        });
        console.log('Klinkut je YES');
        return;
      }
      // Click na iks
      console.log('discarded ' + reason);
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    }  else {
      return  `with: ${reason}`;
    }
  }

  private openModalDeleteCity(contentCity) {
    this.modalService.open(contentCity).result.then((result) => {
    }, (reason) => {
      if (reason === 'YES') {
        this.cityService.deleteCity(this.cityId).subscribe((res) => {
          this.flashMessages.show('You deleted a city!', {cssClass: 'alert-success', timeout: 5000});
          this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(() =>
this.router.navigate(['/cityList']));
        }, (err) => {
          this.flashMessages.show('Error. Try again!', {cssClass: 'alert-danger', timeout: 5000});
        });
        console.log('Klinkut je YES');
        return;
      }
    });
  }

  openModalUpdateCity(contentCityUpdate) {
    this.modalService.open(contentCityUpdate).result.then((result) => {
    }, (err) => {
         // this.flashMessages.show('Error. Try again!', {cssClass: 'alert-danger', timeout: 5000});
          return;
        });
        return;
      }

  onCityUpdateSubmit(values) {
    if (values.name) {
      this.city.name = values.name;
    }
    if (values.country) {
      this.city.country = values.country;
    }
    if (values.information) {
      this.city.information = values.information;
    }
    if (values.coordinates.lat) {
      this.city.lat = values.coordinates.lat;
    }
    if (values.coordinates.lng) {
      this.city.lng = values.coordinates.lng;
    }

    console.log(this.city);
    this.cityService.updateCity(this.cityId, this.city).subscribe( (res) => {
      this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(() =>
this.router.navigate(['/city/' + this.cityId]));
    }, (err) => {
      this.flashMessages.show('Error. Try again to update a city!', {cssClass: 'alert-danger', timeout: 5000});
    });
  }


  openModalAddPlace(contentAddPlace) {
 
     this.modalService.open(contentAddPlace).result.then((result) => {
     }, (reason) => {
       if (reason === 'YES') {
         console.log('Klinkut je YES');
         return;
       }
       // Click na iks
       console.log('discarded ' + reason);
       this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
     });
   }

   onAddPlaceSubmit(values) {
    this.placesService.addPlace(values.place, this.cityId).subscribe((res) => {
      this.flashMessages.show('You add a place!', {cssClass: 'alert-success', timeout: 5000});
      this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(() =>
this.router.navigate(['/city/' + this.cityId]));
    }, (err) => {
      this.flashMessages.show('Error. Try again!', {cssClass: 'alert-danger', timeout: 5000});
    });

   }

   private openModalDeletePlace(contentPlace, placeid) {
    this.modalService.open(contentPlace).result.then((result) => {
    }, (reason) => {
      if (reason === 'YES') {
        this.placesService.deletePlace(placeid, this.cityId).subscribe((res) => {
          this.flashMessages.show('You deleted a place!', {cssClass: 'alert-success', timeout: 5000});
          this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(() =>
this.router.navigate(['/city/' + this.cityId]));
        }, (err) => {
          this.flashMessages.show('Error. Try again!', {cssClass: 'alert-danger', timeout: 5000});
        });
        return;
      }
    });
  }

  openModalUpdatePlace(contentPlaceUpdate, placeid) {
    this.placeId = placeid;
    this.modalService.open(contentPlaceUpdate).result.then((result) => {
    }, (err) => {
         // this.flashMessages.show('Error. Try again!', {cssClass: 'alert-danger', timeout: 5000});
          return;
        });
        return;
      }

  onPlaceUpdateSubmit(values) {
    console.log(values.place);
    if (values.place) {
      this.place = values.place;
    }

    console.log(this.city);
    this.placesService.updatePlace(this.cityId, this.place, this.placeId).subscribe( (res) => {
      this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(() =>
this.router.navigate(['/city/' + this.cityId]));
    }, (err) => {
      this.flashMessages.show('Error. Try again to update a city!', {cssClass: 'alert-danger', timeout: 5000});
    });
  }



}
