import { HttpClientModule} from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, FormBuilder,  ReactiveFormsModule  } from '@angular/forms';

import { AppComponent } from './app.component';
import { ThemeComponent } from './theme/theme.component';
import { NavigationComponent } from './theme/navigation/navigation.component';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';

import { AlertModule } from 'ngx-bootstrap/alert';
import { HomePageComponent } from './home-page/home-page.component';
import { HomePageLoginComponent } from './home-page-login/home-page-login.component';
import { HomePageRegistrationComponent } from './home-page-registration/home-page-registration.component';
import { HomePageCityListComponent } from './home-page-city-list/home-page-city-list.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AboutComponent } from './about/about.component';
import { CitiesComponent } from './cities/cities.component';
import { CitiesAddCityComponent } from './cities-add-city/cities-add-city.component';
import { FooterComponent } from './theme/footer/footer.component';

import {FlashMessagesModule} from 'angular2-flash-messages';
import {AuthService} from './services/auth/auth.service';
import { CookieService } from 'ngx-cookie-service';
import {AuthGuardService} from './services/auth-guard/auth-guard.service';
import {CityService} from './services/city/city.service';
import {AuthGuardAdminService} from './services/auth-guard-admin/auth-guard-admin.service';
import {CommentsService} from './services/comments/comments.service';
import {PlacesService} from './services/places/places.service';

import { BarRatingModule } from 'ngx-bar-rating';


import { AgmCoreModule } from '@agm/core';
import { CitiesAddCommentComponent } from './cities-add-comment/cities-add-comment.component';
import {NgbModal, ModalDismissReasons, NgbModule, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { AdminComponent } from './admin/admin.component';
import {AdminService} from './services/admin/admin.service';
import { GalleryComponent } from './gallery/gallery.component';
import {GalleryService} from './services/gallery/gallery.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { ModalGalleryModule } from 'angular-modal-gallery';
import 'hammerjs';
import 'mousetrap';


const appRoutes: Routes = [
  {
    path: 'home',
    component: HomePageComponent,
    data: { title: 'Home' }
  },
  {
    path: 'cityList',
    component: HomePageCityListComponent,
    data: {title: 'City list'}
  },
  {
    path: 'login',
    component: HomePageLoginComponent,
    data: { title: 'Login' }
  },
  {
    path: 'register',
    component: HomePageRegistrationComponent,
    data: { title: 'Register' }
  },
  {
    path: 'city/:id',
    canActivate: [AuthGuardService],
    component: CitiesComponent,
    data: { title: 'City' }
  },
  {
    path: 'admin',
    canActivate: [AuthGuardService],
    component: AdminComponent,
    data: { title: 'Admin' }
  },
  {
    path: 'city/:id/photo',
    canActivate: [AuthGuardService],
    component: GalleryComponent,
    data: { title: 'Gallery' }
  },
  {
    path: 'addCity',
    canActivate: [AuthGuardService, AuthGuardAdminService],
    component: CitiesAddCityComponent,
    data: { title: 'Add city' }
  },
  {
    path: 'about',
    canActivate: [AuthGuardService],
    component: AboutComponent,
    data: {title: 'About'}
  },
  {
    path: 'city/:id/reviews',
    canActivate: [AuthGuardService],
    component: CitiesAddCommentComponent,
    data: {title: 'Add review'}
  },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ThemeComponent,
    NavigationComponent,
    HomePageComponent,
    HomePageLoginComponent,
    HomePageRegistrationComponent,
    HomePageCityListComponent,
    PageNotFoundComponent,
    AboutComponent,
    CitiesComponent,
    CitiesAddCityComponent,
    FooterComponent,
    CitiesAddCommentComponent,
    AdminComponent,
    GalleryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    CollapseModule.forRoot(),
    FlashMessagesModule.forRoot(),
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDqhIpPGWR6Z6jLv5uG_ISdU_eZXZvoBM4'
    }),
    ModalGalleryModule.forRoot(),
    NgbModule.forRoot(),
    BarRatingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [NgbActiveModal, GalleryService, PlacesService, AdminService, AuthService, CookieService, AuthGuardService, FormBuilder, CityService, AuthGuardAdminService, CommentsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
