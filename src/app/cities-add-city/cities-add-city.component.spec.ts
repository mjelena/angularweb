import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitiesAddCityComponent } from './cities-add-city.component';

describe('CitiesAddCityComponent', () => {
  let component: CitiesAddCityComponent;
  let fixture: ComponentFixture<CitiesAddCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitiesAddCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitiesAddCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
