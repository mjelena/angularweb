import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators, AbstractControl, FormBuilder} from '@angular/forms';
import {CityService} from '../services/city/city.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {CityValidator} from '../services/validation/city.validator';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cities-add-city',
  templateUrl: './cities-add-city.component.html',
  styleUrls: ['../../public/css/main.css', '../../public/css/util.css']
})
export class CitiesAddCityComponent implements OnInit {

  public formAddCity: FormGroup;
  public name: AbstractControl;
  public country: AbstractControl;
  public information: AbstractControl;
  public lat: AbstractControl;
  public lng: AbstractControl;
  public coordinates: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private cityService: CityService,
              private router: Router,
              private flashMessages: FlashMessagesService) {
    this.formAddCity = this.formBuilder.group({
      name:  new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(70), CityValidator.validate])),
      country:  new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(70), CityValidator.validate])),
      information:  new FormControl('', Validators.compose([Validators.maxLength(3000)])),
      coordinates: this.formBuilder.group({
        lat: new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(10)])),
        lng: new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(10)])),
      })
    });
    this.name = this.formAddCity.controls['name'];
    this.country = this.formAddCity.controls['country'];
    this.information = this.formAddCity.controls['information'];
    this.coordinates = <FormGroup> this.formAddCity.controls['coordinates'];
    this.lat = this.coordinates.controls['lat'];
    this.lng = this.coordinates.controls['lng'];
  }

  ngOnInit() {
  }

  onCityAddSubmit(values: any) {
    const city = {
      name: values.name,
      country: values.country,
      information: values.information,
      lat: values.coordinates.lat,
      lng: values.coordinates.lng
    };
    console.log('Doslo do metode.');
    this.cityService.addCity(city).subscribe((response) => {
        console.log('Success Response' + response);
      },
      (err) => {
        this.flashMessages.show('Error. Please try again.', {cssClass: 'alert-danger', timeout: 5000});
        this.router.navigate(['/addCities']);
      },
      () => {
        this.flashMessages.show('You add a city!', {cssClass: 'alert-success', timeout: 5000});
        this.router.navigate(['/cityList']);
      });
  }
}
