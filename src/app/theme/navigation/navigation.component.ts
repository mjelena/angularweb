import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {Router} from '@angular/router';


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['../../../public/css/style.css']
})
export class NavigationComponent implements OnInit {

  constructor(private authService: AuthService,
              private flashMessages: FlashMessagesService,
              private router: Router,
              ) {}

    loggedIn() {
      return this.authService.loggedIn();
    }
    loggedAdmin() {
    return this.authService.loggedAdmin();
    }

  ngOnInit() {
  }



  collapsed(event: any): void {
    console.log(event);
  }

  expanded(event: any): void {
    console.log(event);
  }

  onLogoutClick() {
    this.authService.logout();
    this.flashMessages.show('You are now logged out!', {cssClass: 'alert-success', timeout: 5000});
    this.router.navigate(['/login']);
    return false;
  }

}
