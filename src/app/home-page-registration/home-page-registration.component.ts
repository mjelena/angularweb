import { Component, OnInit, Input } from '@angular/core';
import {FlashMessagesService} from 'angular2-flash-messages/module/flash-messages.service';
import {FormGroup, FormControl, Validators, AbstractControl, FormBuilder} from '@angular/forms';
import {AuthService} from '../services/auth/auth.service';
import {Router} from '@angular/router';
import {EmailValidator, PasswordsMatchValidator, PasswordValidator, NoWhitespaceValidator} from '../services/validation/index';


@Component({
  selector: 'app-home-page-registration',
  templateUrl: './home-page-registration.component.html',
  styleUrls: ['../../public/css/main.css', '../../public/css/util.css']
})
export class HomePageRegistrationComponent implements OnInit {
  private apiUrl = 'http://localhost:3000/api/register/';
  private hostUrl = 'http://localhost:4200/';

  public formRegister: FormGroup;
  public username: AbstractControl;
  public email: AbstractControl;
  admin: Boolean;
  public passwords: FormGroup;
  public password: AbstractControl;
  public passwordConfirm: AbstractControl;



  constructor(private  flashMessages: FlashMessagesService,
              private authService: AuthService,
              private router: Router,
              private formBuilder: FormBuilder,
              ) {
    this.formRegister = this.formBuilder.group({
      username:  new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(15), NoWhitespaceValidator.validate ])),
      email:  new FormControl('', Validators.compose([Validators.required, EmailValidator.validate])),
      passwords: this.formBuilder.group({
      password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(20), PasswordValidator.validate])),
      passwordConfirm: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(20), PasswordValidator.validate])),
    }, {validator: PasswordsMatchValidator.validate('password', 'passwordConfirm')})});

    this.username = this.formRegister.controls['username'];
    this.email = this.formRegister.controls['email'];
    this.passwords = <FormGroup> this.formRegister.controls['passwords'];
    this.password = this.passwords.controls['password'];
    this.passwordConfirm = this.passwords.controls['passwordConfirm'];
}

  ngOnInit() {
  }

  onRegisterSubmit(values: any) {
  const user = {
    username: values.username,
    email: values.email,
    password: values.passwords.password,
    passwordConfirm: values.passwords.passwordConfirm
  };

     this.authService.registerUser(user).subscribe((response) => {
        console.log('Success Response' + response);
     },
       (err) => {
         this.flashMessages.show('Register error! Please try again!', {cssClass: 'alert-danger', timeout: 5000});
         this.router.navigate(['/register']);
    },
       () => {
         this.authService.loginUser(user).subscribe((res) => {
           this.authService.storeUserToken(res['username'], res['token'], res['admin'], res['mainAdmin']);
         }, (error) => {
          this.flashMessages.show('Login error! Please try again!!', {cssClass: 'alert-danger', timeout: 5000});
          this.router.navigate(['/login']);
         });
        this.router.navigate(['/cityList']);
       });

  }

}

