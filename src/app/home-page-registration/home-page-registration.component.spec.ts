import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePageRegistrationComponent } from './home-page-registration.component';

describe('HomePageRegistrationComponent', () => {
  let component: HomePageRegistrationComponent;
  let fixture: ComponentFixture<HomePageRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePageRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
