import { Component, OnInit } from '@angular/core';
import {FlashMessagesService} from 'angular2-flash-messages/module/flash-messages.service';
import {AuthService} from '../services/auth/auth.service';
import {Router} from '@angular/router';
import { HttpResponse} from '@angular/common/http';
import {FormGroup, FormControl, Validators, AbstractControl, FormBuilder} from '@angular/forms';
import {EmailValidator, PasswordsMatchValidator, PasswordValidator, NoWhitespaceValidator} from '../services/validation/index';

@Component({
  selector: 'app-home-page-login',
  templateUrl: './home-page-login.component.html',
  styleUrls: ['../../public/css/main.css', '../../public/css/util.css']
})
export class HomePageLoginComponent implements OnInit {

  public username: AbstractControl;
  public password: AbstractControl;
  public formLogin: FormGroup;


  constructor(private authService: AuthService,
              private flashMessages: FlashMessagesService,
              private router: Router,
              private formBuilder: FormBuilder
              ) {
    this.formLogin = this.formBuilder.group({
      username:  new FormControl('', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(15), NoWhitespaceValidator.validate ])),
        password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(20), PasswordValidator.validate]))
      });

    this.username = this.formLogin.controls['username'];
    this.password = this.formLogin.controls['password'];
  }

  ngOnInit() {
  }

  onLoginSubmit(values: any) {
    const user = {
      username: values.username,
      password: values.password
    };
      this.authService.loginUser(user).subscribe((response) => {
          console.log('Success Response' + response);
          console.log(response['username']);
          console.log(response['token']);
          this.authService.storeUserToken(response['username'], response['token'], response['admin'], response['mainAdmin']);

        },
        (err) => {
          this.flashMessages.show('Login error! Please try again!!', {cssClass: 'alert-danger', timeout: 5000});
          this.router.navigate(['/login']);
        },
        () => {

          this.flashMessages.show('You are now logged in ! Enjoy!!', {cssClass: 'alert-success', timeout: 5000});
          this.router.navigate(['/cityList']);
        });
  }

}
