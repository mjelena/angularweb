# Opis

Web aplikacija ima za cilj da pomogne turistima u obilaženju gradova.
Korisnici mogu da pronadju osnovne informacije o gradu, o mjestima koje trebaju posjetiti, kao i da postavljaju
svoje slike iz obilaska.
Mjesta koja su im se najviše svidjela mogu da ocijene tako da bi drugi korisnici imali bolji uvid o zanimljivosti odredjenih
mjesta u gradu.

# Tehnologije
MEAN stack (MongoDB, Express, AngularJS, Node.js)

Web aplikacija ima frontend i backend validaciju, autentifikaciju i autorizaciju, kao i odgovarajuci error handling.

# AngularWeb

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.10.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
